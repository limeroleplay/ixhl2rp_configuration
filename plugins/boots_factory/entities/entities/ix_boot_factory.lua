local PLUGIN = PLUGIN

ENT.Type = "anim"
ENT.PrintName = "Boot Station"
ENT.Category = "Boot Factory"
ENT.Spawnable = false

if (SERVER) then
	function ENT:Initialize()

		self:SetMoveType(MOVETYPE_NONE)
		self:SetSolid(SOLID_VPHYSICS)
		self:PhysicsInit(SOLID_VPHYSICS)

		local physObj = self:GetPhysicsObject()

		if (IsValid(physObj)) then
			physObj:EnableMotion(false)
			physObj:Sleep()
		end
	end

    function ENT:Use(activator)
        ix.item.spawn(
            "Boot",
            activator:GetPos() + Vector(0,0,72)
        )
    end
else
	function ENT:Draw()
		self:DrawModel()
	end
end
